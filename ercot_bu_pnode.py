import logging
import pandas as pd
import os
import time
import sys
import glob
import bu_alerts
import numpy as np
from zipfile import ZipFile
from datetime import datetime, timedelta
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from bulog import upload_df_to_sf,delete_all_files,unzip_downloaded_files,get_downloaded_files,check_records_by_posted_date,update_index_group_auctionname_postedDate
from winregistry import WinRegistry as Reg
from selenium.webdriver.support.select import Select
import config as config

for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)

job_id=np.random.randint(1000000,9999999)

iso = 'ERCOT'
# Logging configuration
log_file_location = config.LOG_FILE.format(iso)
if os.path.isfile(log_file_location):
    os.remove(log_file_location)

logging.basicConfig(
    level=logging.INFO, 
    format='%(asctime)s [%(levelname)s] - %(message)s',
    filename=log_file_location)

download_path = config.DOWNLOAD_PATH.format(iso.lower())+'\\'
# Tablename 
tablename = config.TABLENAME.format(iso)

lst_status = []

def get_df(file_name:str, posted_date:str, auction_type:str,raw_name:str):
    try:
        #get the file name from file path
        str_file_name = file_name.split('\\')[-1]
        #read the file and convert into df
        temp_df = pd.read_csv(file_name)
        if 'Monthly' in str_file_name:
            # raw_name = str_file_name.split('SourcesAndSinks_')[1].split('_AUCTION')[0].split('.')
            year = raw_name.split('.')[0]
            month = str(config.month_shortnames_dict[raw_name.split('.')[1]]).zfill(2)
            auction_round = 1
            auction_name = iso +' '+ year +'-'+ month + ' Monthly Auction'
        if 'Annual' in str_file_name:
            raw_name = raw_name.replace('.',' ').replace('Annual','Annual ').replace('Seq','Seq ')
            auction_round = raw_name[-1]
            auction_name = iso +' '+ raw_name

        temp_df.rename(columns={'Name':'ISO_PNODE','PriceNode':'PRICE_NODE','BusName':'BUS_NAME','ParticipationFactor':'PARTICIPATION_FACTOR'}, inplace = True)
        temp_df['AUCTION_NAME'] = auction_name
        temp_df['AUCTION_ROUND'] = auction_round
        temp_df['AUCTION_TYPE'] = auction_type.upper()
        temp_df['ISO_POSTED_DATE'] = posted_date
        temp_df['ISO_FILE_NAME'] = str_file_name
        temp_df['INSERT_DATE'] = datetime.now().strftime('%Y-%m-%d')
        temp_df['INDEX'] = ''
        #reordring
        reorder_cols = ['AUCTION_TYPE','AUCTION_NAME','AUCTION_ROUND','ISO_PNODE','PRICE_NODE','BUS_NAME','PARTICIPATION_FACTOR','ISO_POSTED_DATE','ISO_FILE_NAME','INSERT_DATE','INDEX']
        temp_df = temp_df[reorder_cols]        
        return temp_df
    except Exception as e:
        print("Exception caught in get_df() : ",e)
        logging.exception(f'Exception caught in get_df() : {e}')
        raise e

def download_raw_data_files(driver, download_path:str, auction_type:str):
    try:   
        dict_status = {}
        if auction_type == 'MONTHLY':
            auction_select = driver.find_element_by_xpath('//*[@id="_termSelectDiv"]/select[@name="marketTypeTerm"]/option[text()="Monthly Auction"]')
            auction_select.click()
        elif auction_type == 'ANNUAL':
            auction_select = driver.find_element_by_xpath('//*[@id="_termSelectDiv"]/select[@name="marketTypeTerm"]/option[text()="Annual Auction"]')
            auction_select.click()
        time.sleep(2)
        select = Select(driver.find_element_by_id('_marketSelect'))
        raw_auction_name = select.first_selected_option.text
        # Check the source and sinks check 
        source_sink_checkbox = driver.find_element_by_id('2_62_2_-1')
        source_sink_checkbox.click()
        # Get the posted date 
        posted_date = driver.find_element_by_xpath('//*[@id="lmd_62"]').text
        time.sleep(2)
        # Remove existing files from folder
        delete_all_files(download_path)
        # Click on download button
        download_btn = driver.find_element_by_id('_dldbt')
        download_btn.click()
        time.sleep(10)
        # Unzip downloaded zip file and get the files
        unzip_downloaded_files(download_path)
        # Read the files 
        lst_csv_files = get_downloaded_files(download_path)
        # Convert posted date into yyyy-mm-dd format
        arr_posted_date = posted_date[0:11].split('/')
        posted_date = str(arr_posted_date[2]).strip() +'-'+arr_posted_date[0]+'-'+arr_posted_date[1]
        is_data_exist = check_records_by_posted_date(auction_type, posted_date, config.STAGE_DATABASE, config.STAGE_SCHEMA, tablename)
        if len(is_data_exist) == 0:
            if len(lst_csv_files) >0:
                data_df = pd.DataFrame()
                for file_name in lst_csv_files:
                    temp_df = get_df(file_name, posted_date,auction_type,raw_auction_name)
                    data_df = data_df.append(temp_df)
            else:
                logging.info('No files are avaialbe in download path for {}'.format(auction_type))
            # Upload final dataframe to table 
            if len(data_df)>0:
                inserted_rows = upload_df_to_sf(data_df, config.STAGE_DATABASE, config.STAGE_SCHEMA, tablename)
                index_update = update_index_group_auctionname_postedDate(auction_type,config.STAGE_DATABASE,config.STAGE_SCHEMA,tablename)
                if inserted_rows>0:
                    dict_status['AUCTION_TYPE'] = auction_type
                    dict_status['INSERTED_ROWS'] = inserted_rows
                    dict_status['POSTED_DATE'] = posted_date            
                else:
                    logging.warning('No Data available for insertion for {}.'.format(auction_type))
            else:
                logging.info('{}: No Dataframe data available for {}.'.format(iso,auction_type))  
        else:
            logging.info('{}: Data already exist for {}.'.format(iso,auction_type))  
        return dict_status
    except Exception as e:
        print("Exception caught in download_raw_data_files() : ",e)
        logging.exception(f'Exception caught in download_raw_data_files() : {e}')
        raise e

if __name__ == "__main__":
    try:
        starttime=datetime.now()
        inserted_rows = 0
        emaildf = []
        logging.info('Execution Started')
        logging.warning('{}: Start work at {} ...'.format(iso,starttime.strftime('%Y-%m-%d %H:%M:%S')))
        log_json='[{"JOB_ID": "'+str(job_id)+'","CURRENT_DATETIME": "'+str(datetime.now())+'","BU_PNODE_ISO": "'+iso+'"}]'
        bu_alerts.bulog(process_name = tablename,database=config.DATABASE ,status='Started',table_name = config.STAGE_DATABASE +'.'+config.STAGE_SCHEMA+'.'+tablename, row_count=0, log=log_json,warehouse=config.WAREHOUSE,process_owner=config.PROCESS_OWNER)
        #set the ercot client certificate as default in registry key
        reg = Reg()
        # dict_key = reg.read_key(config.REG_PATH)
        str_cert_value = '{"pattern":"https://mis.ercot.com","filter":{"ISSUER":{"CN":"Electric Reliability Council of Texas, Inc. CA"},"SUBJECT":{"CN":"Srishti Sharma"}}}'
        # reg.write_value(config.REG_PATH, '1', str_cert_value, 'REG_SZ')
        # firefox_driver_path = os.getcwd() + '\\geckodriver.exe'
        # # chrome_driver_path =  r"S:\IT Dev\Production_Environment\chromedriver\chromedriver.exe"
        # download_path = os.getcwd() + '\\ercot_raw_file_download\\'
        # firefoxOptions = webdriver.FirefoxOptions()
        # prefs = {'download.default_directory' : download_path}
        # # firefoxOptions.add_argument('prefs', prefs)
        # driver = webdriver.Firefox(executable_path=firefox_driver_path, options=firefoxOptions)  
        # chromeOptions = webdriver.ChromeOptions()
        # prefs = {'download.default_directory' : download_path}
        # chromeOptions.add_experimental_option('prefs', prefs)
        # driver = webdriver.Chrome(executable_path=config.CHROME_DRIVER_PATH, options=chromeOptions)
        #open the url into web browser
        # driver.get(config.ERCOT_URL)
        # time.sleep(2)
        files_location = os.getcwd()+'\\ercot_raw_file_download\\'
        options = Options()
        options.headless = False
        fp = webdriver.FirefoxProfile()
        mime_types = ['application/pdf', 'text/plain', 'application/vnd.ms-excel', 'text/csv', 'application/csv', 'text/comma-separated-values','application/download', 'application/octet-stream', 'binary/octet-stream', 'application/binary', 'application/x-unknown']
        fp.set_preference("browser.download.folderList", 2)
        fp.set_preference("browser.download.manager.showWhenStarting", False)
        fp.set_preference("browser.download.dir", files_location)
        fp.set_preference("browser.helperApps.neverAsk.saveToDisk",",".join(mime_types))
        fp.set_preference("browser.helperApps.neverAsk.openFile", "application/pdf, application/octet-stream, application/x-winzip, application/x-pdf, application/x-gzip")
        fp.set_preference("pdfjs.disabled", True)
        fp.set_preference("security.default_personal_cert", "Select Automatically")
        fp.set_preference('browser.aboutConfig.showWarning', False)
        fp.accept_untrusted_certs = True
        fp.update_preferences()
        # browser = webdriver.Firefox(firefox_profile=fp, options=options, executable_path="D:\\python_practice\geckodriver.exe")
        executable_path = os.getcwd() + '\\geckodriver.exe'
        driver = webdriver.Firefox(firefox_profile=fp,options=options, executable_path=executable_path)
        driver.get(config.ERCOT_MARKET_URL)
        #click on market tab
        market_tab = driver.find_element_by_xpath('//*[@id="_mainTabDiv"]/ul/li[3]')
        market_tab.click()
        time.sleep(5)
        print("After market tab")
        logging.info('**************************** ANNUAL AUCTION PROCESS START **********************')
        dict_status = download_raw_data_files(driver, download_path, 'ANNUAL')
        if dict_status is not None and len(dict_status)>0:
            lst_status.append(dict_status)
        dict_status = download_raw_data_files(driver, download_path, 'MONTHLY') 
        if dict_status is not None and len(dict_status)>0:
            lst_status.append(dict_status)
        if len(lst_status) > 0:
            for dict_status in lst_status:
                if (int(dict_status['INSERTED_ROWS']) > 0):
                    inserted_rows = inserted_rows + int(dict_status['INSERTED_ROWS'])
                    emaildf.append((str(dict_status['AUCTION_TYPE']), str(dict_status['INSERTED_ROWS']),str(dict_status['POSTED_DATE'])))
            senddf = pd.DataFrame(emaildf, columns=["AUCTION_TYPE", "INSERTED_ROWS", "POSTED_DATE"])
            logging.info(f'Data Insertion Details \n\n {senddf} \n\n')
            if len(senddf)>0:
                msgbody=config.file_upload_msgbody + '<tr>'
                msgbody=msgbody+'<td style="font-weight: bold; text-align:center;">AUCTION_TYPE</td>'
                msgbody=msgbody+'<td style="font-weight: bold; text-align:center;">INSERTED_ROWS</td>'
                msgbody=msgbody+'<td style="font-weight: bold; text-align:center;">POSTED_DATE</td>'
                msgbody=msgbody+'</tr>'
                for i, x in senddf.iterrows():
                    msgbody=msgbody+'<tr>'
                    msgbody=msgbody+'<td style="font-weight:normal; text-align:center;">{}</td>'.format(x['AUCTION_TYPE'])
                    msgbody=msgbody+'<td style="font-weight:normal; text-align:center;">{}</td>'.format(x['INSERTED_ROWS'])
                    msgbody=msgbody+'<td style="font-weight:normal; text-align:center;">{}</td>'.format(x['POSTED_DATE'])
                    msgbody=msgbody+'</tr>'
                msgbody=msgbody+'</table>'
                bu_alerts.send_mail(
                    receiver_email = config.RECEIVER_EMAIL,
                    mail_subject = config.file_upload_subject.format(tablename),
                    mail_body = msgbody.format(tablename),
                    attachment_location = log_file_location
                )
        logging.info('Execution Done')
        log_json='[{"JOB_ID": "'+str(job_id)+'","CURRENT_DATETIME": "'+str(datetime.now())+'","BU_PNODE_ISO": "'+iso+'"}]'
        bu_alerts.bulog(process_name= tablename,database = config.DATABASE,status='Completed',table_name = config.STAGE_DATABASE +'.'+config.STAGE_SCHEMA+'.'+tablename, row_count = inserted_rows, log=log_json,warehouse=config.WAREHOUSE,process_owner=config.PROCESS_OWNER)
        bu_alerts.send_mail(
                receiver_email = config.RECEIVER_EMAIL,
                mail_subject = config.success_subject.format(tablename),
                mail_body = config.success_mail_body.format(tablename),
                attachment_location = log_file_location
        )
    except Exception as ex:
        print("Exception caught during execution: ",ex)
        logging.exception(f'Exception caught during execution: {ex}')
        log_json='[{"JOB_ID": "'+str(job_id)+'","CURRENT_DATETIME": "'+str(datetime.now())+'","BU_PNODE_ISO": "'+iso+'"}]'
        bu_alerts.bulog(process_name= tablename,database = config.DATABASE,status='Failed',table_name = config.STAGE_DATABASE +'.'+config.STAGE_SCHEMA+'.'+tablename, row_count=0, log=log_json,warehouse=config.WAREHOUSE,process_owner=config.PROCESS_OWNER) 
        bu_alerts.send_mail(
            receiver_email = config.RECEIVER_EMAIL,
            mail_subject=config.failure_subject.format(tablename),
            mail_body=config.failure_mail_body.format(tablename),
            attachment_location = log_file_location
        )
        sys.exit(1)
    finally:
        driver.close()
    endtime=datetime.now()
    logging.warning('Complete work at {} ...'.format(endtime.strftime('%Y-%m-%d %H:%M:%S')))
    logging.warning('Total time taken: {} seconds'.format((endtime-starttime).total_seconds()))