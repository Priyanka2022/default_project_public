import os

# Database configurations
WAREHOUSE = 'ITPYTHON_WH'
PROCESS_OWNER = 'RADHA'
DATABASE = 'POWERDB'
SCHEMA = 'PTEST'
# STAGE_DATABASE = 'POWERSTAGE'
STAGE_DATABASE = 'POWERDB_DEV'
STAGE_SCHEMA = 'PDIM'
TABLENAME = 'BU_PNODE_{}_RAW_FILE'
DOWNLOAD_PATH = os.getcwd() + '\\{}_raw_file_download'

LOG_FILE = os.getcwd() + '\\logs\\' + '{}_LOG.txt'
# Month names and numbers in key value pair 

month_shortnames_dict = {'JAN':1,'FEB':2,'MAR':3,'APR':4,'MAY':5,'JUN':6,'JUL':7,'AUG':8,'SEP':9,'OCT':10,'NOV':11,'DEC':12}
month_fullnames_dict = {'January':1,'February':2,'March':3,'April':4,'May':5,'June':6,'July':7,'August':8,'September':9,'October':10,'November':11,'December':12}


# Initalize chrome driver
# CHROME_DRIVER_PATH = os.getcwd() + '\\chromedriver.exe'
# CHROME_DRIVER_PATH = r"S:\IT Dev\Production_Environment\chromedriver\chromedriver.exe"
# CHROME_DRIVER_PATH = os.getcwd() + '\\RAW_TABLES\\chromedriver.exe'



# Email configurations
RECEIVER_EMAIL = 'radha.waswani@biourja.com,priyanka.solanki@biourja.com'
# RECEIVER_EMAIL = 'indiapowerit@biourja.com,DApower@biourja.com'

# New file upload mail content
file_upload_subject = "{} : New file upload details"
msgbody='<br/>{} New file upload details: <br/>'
file_upload_msgbody = msgbody+'<br/><table border = 1>'

success_subject = "JOB SUCCESS - {}"
success_mail_body = '{} completed successfully, Attached logs'

failure_subject = "JOB FAILED - {}"
failure_mail_body = '{} failed during execution, Attached logs'
# Certificate registry path 
REG_PATH = r'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Chrome\AutoSelectCertificateForUrls'
# SPPISO login credentials
USERNAME = "srishti.sharma@biourja.com"
PASSWORD = "Biourja@2021"

# URLs

CAISO_URL =  'http://www.caiso.com/Pages/documentsbygroup.aspx?GroupID=F401117F-1E07-485A-880B-FFFF19314DBF'
# ERCOT_URL = 'https://mis.ercot.com/pps/tibco/mis/Pages/Market+Information/CRR'
ERCOT_MARKET_URL = 'https://mis.ercot.com/mui-ercot-ihedge/download/public/main.htm#ui-tabs-3'
MISO_URL = 'https://markets.midwestiso.org/mui-miso-ihedge/#/download/public'
SPPISO_URL = 'https://uaa.spp.org/auth-login?target=https://marketplace.spp.org'
SPPISO_TCR_URL = 'https://tcr.spp.org/tcr/'
SPPISO_MARKET_URL = 'https://tcr.spp.org/tcr/download/public/main.htm'
# NYISO_URL = 'https://www.nyiso.com/search?p_p_id=gsearchportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=get_search_results&p_p_cacheability=cacheLevelPage&q=Attachment%20E%20POI%20and%20POW%202019&sortField=_score'
NYISO_URL = 'https://www.nyiso.com/search?p_p_id=gsearchportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=get_search_results&p_p_cacheability=cacheLevelPage&q=att-e&start=0&sortField=newest'
NEISO_URL = 'https://www.iso-ne.com/markets-operations/settlements/pricing-node-tables/'


# PJMISO auction type URLs 
pjm_ftr_prompt_link = 'https://www.pjm.com/pub/account/auction-user-info/downloads/ftr-source-sink-prompt.csv'
pjm_ftr_nonprompt_link = 'https://www.pjm.com/pub/account/auction-user-info/downloads/ftr-source-sink-nonprompt.csv'
pjm_ftr_annual_longterm_link = 'https://www.pjm.com/markets-and-operations/ftr.aspx'