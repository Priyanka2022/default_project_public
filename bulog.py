# -*- coding: utf-8 -*-
"""
Created Date: 16 Dec 2020
@author: Manish Gupta
"""
import datetime as dt
import socket
import json
import os
import snowflake.connector
import logging
import glob
from zipfile import ZipFile
import sys
sys.path.append(r'{}'.format(os.getcwd()))
import config as config
from bu_snowflake import get_connection


# Logging configurations
logging.basicConfig(level=logging.INFO)
warehouse = config.WAREHOUSE

    
def bulog(process_name, status, table_name, row_count, log, warehouse):
    try:
        log2=json.loads(log)
        print(log2)
    except ValueError:
        log='[{"Error":"Invalid JSON String"}]'
        
    sql='''
        insert into powerdb.plog.process_log 
        select column1,column2,column3,column4,column5,column6, parse_json(column7), column8
        from values('{}','{}','{}','{}','{}','{}','{}','{}')
        '''
    processed_by=socket.gethostname()
    starttime=dt.datetime.now()
    
    try:
        sql=sql.format(process_name, processed_by, starttime, status, table_name, row_count, log, warehouse)
        conn=get_connection(role='OWNER_{}'.format(databasename),
                              database=databasename, schema=schemaname)
        conn.cursor().execute('USE WAREHOUSE {}'.format(warehouse))
        conn.cursor().execute(sql)
        conn.close()
    except:
        pass
    
# Check the record existance on the basis of file name and iso type
def check_records_existance(file_name:str, auction_type:str, databasename:str, schemaname:str, tablename:str):
    try:
        full_table_name = '{}.{}.{}'.format(databasename, schemaname, tablename)
        conn = get_connection(role='OWNER_{}'.format(databasename),
                              database=databasename, schema=schemaname)
        conn.cursor().execute('USE WAREHOUSE {}'.format(warehouse))
        data_exist_query =f'''select * from {full_table_name} where AUCTION_TYPE='{auction_type}' and ISO_FILE_NAME ='{file_name}' '''
        records = conn.cursor().execute(data_exist_query).fetchall()
        conn.close()
        return records
    except Exception as e: 
        logging.error('Exception caught in check_records_existance(): '+str(e))
        print('Exception caught in check_records_existance(): '+str(e))
        raise e

def check_records_existence_file_posted_date(file_name:str, auction_type:str, databasename:str, schemaname:str, tablename:str,posted_date:str):
    try:
        full_table_name = '{}.{}.{}'.format(databasename, schemaname, tablename)
        conn = get_connection(role='OWNER_{}'.format(databasename),
                              database=databasename, schema=schemaname)
        conn.cursor().execute('USE WAREHOUSE {}'.format(warehouse))
        if posted_date is not None:
            data_exist_query =f'''select * from {full_table_name} where AUCTION_TYPE='{auction_type}' and ISO_FILE_NAME ='{file_name}' and ISO_POSTED_DATE = '{posted_date}' '''
        else:
            data_exist_query =f'''select * from {full_table_name} where AUCTION_TYPE='{auction_type}' and ISO_FILE_NAME ='{file_name}' '''
        records = conn.cursor().execute(data_exist_query).fetchall()
        conn.close()
        return records
    except Exception as e: 
        logging.error('Exception caught in check_records_existence_file_posted_date(): '+str(e))
        print('Exception caught in check_records_existence_file_posted_date(): '+str(e))
        raise e

#Check the record existance on the basis of file name and iso type
def check_records_by_posted_date(auction_type:str,posted_date:str, databasename:str, schemaname:str, tablename:str):
    try:
        full_table_name = '{}.{}.{}'.format(databasename, schemaname, tablename)
        conn = get_connection(role='OWNER_{}'.format(databasename),
                              database=databasename, schema=schemaname)
        data_exist_query =f'''select * from {full_table_name} where ISO_POSTED_DATE='{posted_date}' and AUCTION_TYPE='{auction_type}' '''
        conn.cursor().execute('USE WAREHOUSE {}'.format(warehouse))
        records = conn.cursor().execute(data_exist_query).fetchall()
        conn.close()
        return records
    except Exception as e: 
        logging.error('Exception caught in check_records_by_posted_date(): '+str(e))
        print('Exception caught in check_records_by_posted_date(): '+str(e))
        raise e

def check_records_by_postedDate(posted_date:str, databasename:str, schemaname:str, tablename:str):
    try:
        full_table_name = '{}.{}.{}'.format(databasename, schemaname, tablename)
        conn = get_connection(role='OWNER_{}'.format(databasename),
                              database=databasename, schema=schemaname)
        conn.cursor().execute('USE WAREHOUSE {}'.format(warehouse))
        data_exist_query =f'''select * from {full_table_name} where ISO_POSTED_DATE='{posted_date}' '''
        records = conn.cursor().execute(data_exist_query).fetchall()
        conn.close()
        return records
    except Exception as e: 
        logging.error('Exception caught in check_records_by_postedDate(): '+str(e))
        print('Exception caught in check_records_by_postedDate(): '+str(e))
        raise e
def remove_files_from_folder(folder_location:str):
    try:
        files = glob.glob(folder_location+'//*')
        for f in files:
            os.remove(f)
    except Exception as e: 
        logging.error('Exception caught in remove_files_from_folder(): '+str(e))
        print('Exception caught in remove_files_from_folder(): '+str(e))
        raise e
    
#Upoload csv file into snowflake database
def upload_df_to_sf(df, databasename:str, schemaname:str, tablename:str):
    """
    Saves dataframe as csv and uploads to Snowflake.
    Args:
        df : the dataframe in question
        databasename : the database of the destination in Snowflake
        schemaname : the schema of the destination in Snowflake
        tablename : the table of the destination in Snowflake
    Returns:
        1 : the dataframe was successfully loaded to Snowflake
        or
        None : wherein the columns of the dataframe and the table in Snowflake
               do not share the same columns
    """
    sf_connection = get_connection(role='OWNER_{}'.format(databasename),
                              database=databasename, schema=schemaname)
    # csv_file = os.getcwd() + '\\' "to_snowflake.csv"
    csv_file = "to_snowflake_{}.csv".format(tablename)
    print(df.head(10))
    df.to_csv(csv_file, index=False, date_format="%Y-%m-%d %H:%M:%S")
    print("Convert df into csv file")
    sf_connection.cursor().execute('USE WAREHOUSE {}'.format(warehouse))
    print("USE WAREHOUSE ITPYTHON_WH")
    sf_connection.cursor().execute("USE DATABASE {}".format(databasename))
    print("USE DATABASE {}".format(databasename))
    sf_connection.cursor().execute("USE SCHEMA {}".format(schemaname))
    print("USE SCHEMA {}".format(schemaname))
    powerdb_temp_table = "{}.{}.TBL_{}".format(databasename, schemaname, tablename)
    print(powerdb_temp_table)
    powerdb_main_table = "{}.{}.{}".format(databasename, schemaname, tablename)
    print(powerdb_main_table)
    sf_connection.cursor().execute("PUT file://{} @%{} overwrite=true".format(csv_file, tablename))
    sf_connection.cursor().execute('''
    COPY INTO {} file_format=(type=csv
    skip_header=1 field_optionally_enclosed_by = '"' empty_field_as_null=true escape_unenclosed_field=None)
    '''.format(powerdb_main_table))        
    print("Remove csv file from local drive")
    os.remove(csv_file)
    return len(df)

# Update INDEX column for AUCTION TYPE order by ISO_POSTED_DATE then month of AUCTION_NAME
def update_index_order_auctionname_month(auction_type,databasename,schemaname,tablename):
    try:
        if auction_type is not None:
            query_posted_date = f'''select distinct AUCTION_NAME,ISO_POSTED_DATE,SUBSTRING(AUCTION_NAME,12,2) as MONTH from {databasename}.{schemaname}.{tablename} where AUCTION_TYPE='{auction_type.upper()}' 
                group by AUCTION_NAME,ISO_POSTED_DATE order by ISO_POSTED_DATE desc,MONTH desc'''
            conn = get_connection(role='OWNER_{}'.format(databasename),
                              database=databasename, schema=schemaname)
            conn.cursor().execute('USE WAREHOUSE {}'.format(warehouse))
            records = conn.cursor().execute(query_posted_date).fetchall()
            for i in range(0,len(records)):
                auction_name = records[i][0]
                posted_date = records[i][1].strftime('%Y-%m-%d %H:%M:%S')
                if i<6: 
                    print(f"Update INDEX for ISO_POSTED_DATE = {posted_date}")
                    logging.info(f"Update INDEX for ISO_POSTED_DATE = {posted_date}")
                    update_index = r"UPDATE {}.{}.{} set INDEX = {} where ISO_POSTED_DATE='{}' and AUCTION_NAME='{}'".format(databasename,schemaname,tablename,i,posted_date,auction_name)
                else:
                    print(f"Delete records for {posted_date}")
                    print(f"Delete records for {posted_date}")
                    update_index = r"Delete from {}.{}.{} where ISO_POSTED_DATE='{}' and AUCTION_NAME='{}'".format(databasename,schemaname,tablename,posted_date,auction_name)
                affected_rows = conn.cursor().execute(update_index).fetchall()
                print(f"Index updated for {affected_rows} rows")
            conn.close()
        else: 
            records=[]
        return records
    except Exception as ex:
        logging.error('Exception caught in update_index_order_auctionname_month(): '+str(ex))
        print('Exception caught in update_index_order_auctionname_month(): '+str(ex))
        raise ex

# Update INDEX column order by ISO_POSTED_DATE(NEISO,NYISO)
def update_index_order_postedDate(databasename,schemaname,tablename):
    try:
        query_posted_date = f'''select distinct(ISO_POSTED_DATE),ISO_FILE_NAME from {databasename}.{schemaname}.{tablename} order by ISO_POSTED_DATE desc'''
        # query_posted_date = f'''select distinct(AUCTION_NAME),ISO_FILE_NAME,ISO_POSTED_DATE  from {databasename}.{schemaname}.{tablename} where AUCTION_TYPE='{auction_type.upper()}' group by AUCTION_NAME,ISO_FILE_NAME,ISO_POSTED_DATE order by AUCTION_NAME desc'''
        conn = get_connection(role='OWNER_{}'.format(databasename),
                              database=databasename, schema=schemaname)
        conn.cursor().execute('USE WAREHOUSE {}'.format(warehouse))
        records = conn.cursor().execute(query_posted_date).fetchall()
        for i in range(0,len(records)):
            posted_date = records[i][0].strftime('%Y-%m-%d %H:%M:%S')
            if i<3: 
                print(f"Update INDEX for ISO_POSTED_DATE = {posted_date}")
                logging.info(f"Update INDEX for ISO_POSTED_DATE = {posted_date}")
                update_index = r"UPDATE {}.{}.{} set INDEX = {} where ISO_POSTED_DATE='{}'".format(databasename,schemaname,tablename,i,posted_date)
            else:
                print(f"Delete records for {posted_date}")
                print(f"Delete records for {posted_date}")
                update_index = r"Delete from {}.{}.{} where ISO_POSTED_DATE='{}'".format(databasename,schemaname,tablename,posted_date)
            affected_rows = conn.cursor().execute(update_index).fetchall()
            print(f"Index updated for {affected_rows} rows")
        conn.close()
        return records
    except Exception as ex:
        logging.error('Exception caught in update_index_order_postedDate(): '+str(ex))
        print('Exception caught in update_index_order_postedDate(): '+str(ex))
        raise ex

# Update INDEX column for AUCTION TYPE order by ISO_POSTED_DATE(PJMISO)
def update_auction_index_order_postedDate(auction_type,databasename,schemaname,tablename):
    try:
        if auction_type is not None:
            query_posted_date = f'''select distinct(ISO_POSTED_DATE),ISO_FILE_NAME from {databasename}.{schemaname}.{tablename} where AUCTION_TYPE='{auction_type.upper()}' order by ISO_POSTED_DATE desc'''
            # query_posted_date = f'''select distinct(AUCTION_NAME),ISO_FILE_NAME,ISO_POSTED_DATE  from {databasename}.{schemaname}.{tablename} where AUCTION_TYPE='{auction_type.upper()}' group by AUCTION_NAME,ISO_FILE_NAME,ISO_POSTED_DATE order by AUCTION_NAME desc'''
            conn = get_connection(role='OWNER_{}'.format(databasename),
                              database=databasename, schema=schemaname)
            conn.cursor().execute('USE WAREHOUSE {}'.format(warehouse))
            records = conn.cursor().execute(query_posted_date).fetchall()
            for i in range(0,len(records)):
                posted_date = records[i][0].strftime('%Y-%m-%d %H:%M:%S')
                if i<3: 
                    print(f"Update INDEX for ISO_POSTED_DATE = {posted_date}")
                    logging.info(f"Update INDEX for ISO_POSTED_DATE = {posted_date}")
                    update_index = r"UPDATE {}.{}.{} set INDEX = {} where AUCTION_TYPE='{}' and ISO_POSTED_DATE='{}'".format(databasename,schemaname,tablename,i,auction_type.upper(),posted_date)
                else:
                    print(f"Delete records for {posted_date}")
                    print(f"Delete records for {posted_date}")
                    update_index = r"Delete from {}.{}.{} where AUCTION_TYPE='{}' and ISO_POSTED_DATE='{}'".format(databasename,schemaname,tablename,auction_type.upper(),posted_date)
                affected_rows = conn.cursor().execute(update_index).fetchall()
                print(f"Index updated for {affected_rows} rows")
            conn.close()
        else:
            records =[]
        return records
    except Exception as ex:
        logging.error('Exception caught in update_auction_index_order_postedDate(): '+str(ex))
        print('Exception caught in update_auction_index_order_postedDate(): '+str(ex))
        raise ex

def delete_all_files(folder_path:str):
    try:
        files = glob.glob(folder_path+'*')
        if len(files)>0:
            for f in files:
                os.remove(f)
    except Exception as e:
        print("Exception caught in delete_all_files() : ",e)
        logging.exception(f'Exception caught in delete_all_files() : {e}')
        raise e

def get_downloaded_files(folder_path:str):
    try:
        lst_files = []
        files = glob.glob(folder_path+'*.csv')
        for f in files:
            lst_files.append(f)
        return lst_files
    except Exception as e:
        print("Exception caught in get_downloaded_files() : ",e)
        logging.exception(f'Exception caught in get_downloaded_files() : {e}')
        raise e
def unzip_downloaded_files(download_path:str):
    try:
        logging.info (f"Download_path to unzip downloaded files: {download_path} ")
        for root, dirs, files in os.walk(download_path):
            if len(files) > 0:
                for file_name in files:
                    if '.zip' in file_name:
                        with ZipFile(download_path+file_name, 'r') as zipObj:
                            # Extract all the contents of zip file in current directory
                            zipObj.extractall(download_path)
                    else:
                        raise f"Error in unzip the downloaded file:{file_name}"
            else:
                print('No files available')
    except Exception as e:
        print("Exception caught in unzip_downloaded_files() : ",e)
        logging.exception(f'Exception caught in unzip_downloaded_files() : {e}')
        raise e
# Update INDEX column group by AUCTION_NAME,ISO_POSTED_DATE(ERCOT,MISO,SPPISO) 
def update_index_group_auctionname_postedDate(auction_type,databasename,schemaname,tablename):
    try:
        if auction_type is not None:
            query_posted_date = f'''select distinct(AUCTION_NAME),ISO_POSTED_DATE from {databasename}.{schemaname}.{tablename} where AUCTION_TYPE='{auction_type.upper()}' group by AUCTION_NAME,ISO_POSTED_DATE order by ISO_POSTED_DATE desc'''
            conn = get_connection(role='OWNER_{}'.format(databasename),
                              database=databasename, schema=schemaname)
            conn.cursor().execute('USE WAREHOUSE {}'.format(warehouse))
            records = conn.cursor().execute(query_posted_date).fetchall()
            for i in range(0,len(records)):
                auction_name = records[i][0]
                posted_date = records[i][1].strftime('%Y-%m-%d %H:%M:%S')
                if i<3: 
                    print(f"Update INDEX for ISO_POSTED_DATE = {posted_date}")
                    logging.info(f"Update INDEX for ISO_POSTED_DATE = {posted_date}")
                    update_index = r"UPDATE {}.{}.{} set INDEX = {} where ISO_POSTED_DATE='{}' and AUCTION_NAME='{}'".format(databasename,schemaname,tablename,i,posted_date,auction_name)
                else:
                    print(f"Delete records for {posted_date}")
                    print(f"Delete records for {posted_date}")
                    update_index = r"Delete from {}.{}.{} where ISO_POSTED_DATE='{}' and AUCTION_NAME='{}'".format(databasename,schemaname,tablename,posted_date,auction_name)
                affected_rows = conn.cursor().execute(update_index).fetchall()
                print(f"Index updated for {affected_rows} rows")
            conn.close()
        else: 
            records=[]
        return records
    except Exception as ex:
        logging.error('Exception caught in update_index_group_auctionname_postedDate(): '+str(ex))
        print('Exception caught in update_index_group_auctionname_postedDate(): '+str(ex))
        raise ex

def get_file_name_from_url(url:str,iso:str):
    '''
    This function will be retrun the file name from given download url
    '''
    try:   
        if (url.find('/')):
            if iso.lower() =='nyiso':
                file_name = url.split('/')[-2]
            else:
                file_name = url.split('/',1)[1].split('/')[-1]
        return file_name
    except Exception as e:
        print("Exception caught in get_file_name_from_url() : ",e)
        logging.exception(f'Exception caught in get_file_name_from_url() : {e}')
        raise e

